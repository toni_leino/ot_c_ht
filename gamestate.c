#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "gamestate.h"

GameState *GS_alloc (int width, int height)
{
    GameState *tmp = malloc(sizeof(GameState));
    if (tmp == NULL) return NULL;

    /* calloc() initializes to zero, so it's better than malloc() here */
    tmp->board = calloc(width * height, sizeof(unsigned char));
    if (tmp->board == NULL) {
        free(tmp);
        return NULL;
    }

    tmp->width = width;
    tmp->height = height;
    tmp->score = 0;

    return tmp;
}

GameState *GS_init (int width, int height)
{
    GameState *tmp = GS_alloc(width, height);
    if (tmp == NULL) return NULL;

    add_random_tile(tmp);
    add_random_tile(tmp);

    return tmp;
}

GameState *GS_copy (GameState *gs)
{
    GameState *tmp;

    assert(gs != NULL);
    assert(gs->board != NULL);

    tmp = GS_alloc(gs->width, gs->height);
    if (tmp == NULL) return NULL;

    /* make a copy of the board */
    memcpy(tmp->board, gs->board, gs->width * gs->height * sizeof(unsigned char));

    /* GS_alloc() sets width and height, but the score must be set here */
    tmp->score = gs->score;

    return tmp;
}

void GS_free (GameState *gs)
{
    if (gs == NULL) return;

    /* freeing null pointers is okay, so there's no need to check board */
    free(gs->board);
    free(gs);
}

void add_random_tile (GameState *gs)
{
    int i;
    int board_size;

    assert(gs != NULL);
    assert(gs->board != NULL);

    board_size = gs->width * gs->height;

    /* if there are no empty tiles, the game is over and there's an error
     * in the program */
    assert(!is_board_full(gs));

    /* choose a random empty spot */
    for (;;) {
        i = rand() % board_size;
        if (gs->board[i] == 0) break;
    }
    gs->board[i] = (rand() % 2) ? 2 : 1;
}

void add_tile (GameState *gs, int value, int row, int col)
{
    assert(gs != NULL);
    assert(gs->board != NULL);
    assert(value_to_tile(value) != -1);
    assert(row >= 0 && row < gs->height);
    assert(col >= 0 && col < gs->width);
    assert(gs->board[array_index(row, col, gs->width)] == 0);

    gs->board[array_index(row, col, gs->width)] = value_to_tile(value);
}

int is_board_full (const GameState *gs)
{
    int i;
    int board_size;

    assert(gs != NULL);
    assert(gs->board != NULL);

    board_size = gs->width * gs->height;

    for (i = 0; i < board_size; ++i) {
        if (gs->board[i] == 0) {
            /* empty spot found */
            return 0;
        }
    }

    /* no empty spot found */
    return 1;
}

/* Returns non-zero if any of the tile's four neighbors has the same value. */
static int can_combine_with_neighbor (const GameState *gs, int row, int col)
{
    int nrow, ncol;
    int tvalue = gs->board[array_index(row, col, gs->width)];

    assert(gs != NULL);
    assert(gs->board != NULL);

    for (nrow = row - 1; nrow <= row + 1; ++nrow) {
        if (nrow < 0 || nrow >= gs->height) continue;

        for (ncol = col - 1; ncol <= col + 1; ++ncol) {
            if (ncol < 0 || ncol >= gs->width) continue;
            
            /* don't check diagonal neighbors */
            if (nrow != row && ncol != col) continue;

            /* don't check the tile itself */
            if (nrow == row && ncol == col) continue;

            if (gs->board[array_index(nrow, ncol, gs->width)] == tvalue) {
                return 1;
            }
        }
    }

    return 0;
}

int is_game_over (const GameState *gs)
{
    int row, col;

    assert(gs != NULL);

    /* game cannot be over if there are empty spots */
    if (!is_board_full(gs)) {
        return 0;
    }

    /* check if any tile has a tile with the same value next to it */
    for (row = 0; row < gs->height; ++row) {
        for (col = 0; col < gs->width; ++col) {
            if (can_combine_with_neighbor(gs, row, col)) {
                return 0;
            }
        }
    }

    return 1;
}

int tile_value (unsigned char tile)
{
    return 1 << tile;
}

int value_to_tile (int value)
{
    int i = 1;
    while (tile_value(i) < value) ++i;
    if (tile_value(i) == value) {
        return i;
    } else {
        return -1; /* a tile cannot have the given value */
    }
}

int array_index (int row, int col, int width)
{
    return row * width + col;
}
