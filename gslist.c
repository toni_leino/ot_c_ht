#include <stdlib.h>
#include <assert.h>

#include "gamestate.h"
#include "gslist.h"

/* Frees the given node (including the state) and every node and state that
 * come after it. */
static void GSList_destroy_forward (GSLNode *node)
{
    GSLNode *curr = node;
    GSLNode *next;
    
    while (curr != NULL) {
        next = curr->next;
        GS_free(curr->state);
        free(curr);
        curr = next;
    }
}

GSList *GSList_alloc (void)
{
    GSList *tmp = malloc(sizeof(GSList));

    if (tmp == NULL) return NULL;

    tmp->oldest = NULL;
    tmp->newest = NULL;
    tmp->curr   = NULL;

    return tmp;
}


void GSList_destroy (GSList *list)
{
    if (list == NULL) return;

    GSList_destroy_forward(list->oldest);
    free(list);
}

int GSList_add (GSList *list, GameState *state)
{
    GSLNode *tmp = malloc(sizeof(GSLNode));

    if (tmp == NULL) return 1;
    assert(list != NULL);
    assert(state != NULL);

    tmp->state = state;
    tmp->prev  = list->curr;
    tmp->next  = NULL;

    /* empty list? */
    if (list->curr == NULL) {
        assert(list->oldest == NULL && list->newest == NULL);

        list->oldest = tmp;
        list->newest = tmp;
        list->curr   = tmp;

        return 0;
    } else {
        /* free the nodes after curr before adding the new one */
        GSList_destroy_forward(list->curr->next);

        list->curr->next = tmp;
        list->curr       = tmp;
        list->newest     = tmp;

        return 0;
    }
}

int GSList_undo (GSList *list) {
    assert(list != NULL);

    /* empty list? */
    if (list->curr == NULL) {
        assert(list->oldest == NULL && list->newest == NULL);
        return 1;
    }
    
    /* no older states? */
    else if (list->curr == list->oldest) {
        assert(list->curr->prev == NULL);
        return 1;
    }

    else {
        list->curr = list->curr->prev;
        return 0;
    }
}

int GSList_redo (GSList *list) {
    assert(list != NULL);

    /* empty list? */
    if (list->curr == NULL) {
        assert(list->oldest == NULL && list->newest == NULL);
        return 1;
    }
    
    /* no newer states? */
    else if (list->curr == list->newest) {
        assert(list->curr->next == NULL);
        return 1;
    }

    else {
        list->curr = list->curr->next;
        return 0;
    }
}

GameState *GSList_getcurr (const GSList *list)
{
    assert(list != NULL);

    return list->curr->state;
}
