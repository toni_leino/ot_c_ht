/* This file contains the main() function. */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "gamestate.h"
#include "gslist.h"
#include "gameio.h"
#include "gameexec.h"

#define BUFSIZE 256

int main (int argc, char **argv)
{
    char *buf = NULL;
    int quit = 0; /* non-zero if the user wants to quit */
    GSList *list = NULL; /* list of all past (and future) game states */

    /* initialize RNG */
    srand(time(NULL));

    /* main loop */
    while (!quit) {
        Command cmd;

        /* read and parse user input */
        if ((buf = read_line(stdin)) == NULL) {
            if (feof(stdin)) {
                fprintf(stderr, "stdin reached eof\n");
            } else {
                fprintf(stderr, "reading input failed\n");
            }
            quit = 1;
            goto loop_end;
        };
        cmd = input_to_command(buf);

        switch (cmd) {
            case CMD_QUIT:
                quit = 1;
                break;
            case CMD_RESET:
            case CMD_BOARD:
            case CMD_SCORE:
            case CMD_UP:
            case CMD_DOWN:
            case CMD_LEFT:
            case CMD_RIGHT:
            case CMD_UNDO:
            case CMD_REDO:
            case CMD_SAVE:
            case CMD_LOAD:
            case CMD_LIST:
            case CMD_DELETE:
                exec_command(&list, cmd, buf);
                break;
            case CMD_UNKNOWN:
                printf("%s", unknown_cmd_error);
                break;
            default:
                fprintf(stderr, "input_to_command() returned weird value %d\n", cmd);
                break;
        }

loop_end:
        if (quit) {
            if (list != NULL) {
                GSList_destroy(list);
            }
        }
        free(buf);
    }

    return 0;
}
