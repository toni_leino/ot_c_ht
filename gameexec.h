/* Functions that actually execute the user's commands. */

#ifndef GAMEEXEC_H
#define GAMEEXEC_H

#include "gamestate.h"
#include "gslist.h"
#include "gameio.h"

/* Executes the given command on the current state in list. input is the user's
 * full input; some commands have parameters.
 * Returns zero on success. */
int exec_command (GSList **list, Command cmd, const char *input);

#endif /* end of include guard: GAMEEXEC_H */
