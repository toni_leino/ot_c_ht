#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>

#include "gamesave.h"
#include "gameio.h"

/* name of the save file */
static const char *savefile = "2048.sav";

/* name of the save file backup */
static const char *backupfile = "2048.sav.bak";

/* Returns non-zero if a save with the given name already exists. */
static int does_save_exist (const char *name)
{
    char *line = NULL;
    FILE *fp = fopen(savefile, "r");

    assert(name != NULL);
    if (fp == NULL) return 0;

    for (;;) {
        char buf[256];
        line = read_line(fp);

        /* read_line() returns NULL on eof */
        if (line == NULL) {
            fclose(fp);
            return 0;
        }

        sscanf(line, "%255s", buf);
        if (strcmp(name, buf) == 0) {
            fclose(fp);
            free(line);
            return 1;
        }

        free(line);
    }

    fclose(fp);

    return 0;
}

int save_game (const GameState *state, const char *name)
{
    FILE *fp = NULL;
    int i;

    if (!does_save_exist(name)) {
        fp = fopen(savefile, "a");
        if (fp == NULL) return 1;
        fprintf(fp, "%s %ld %d %d", name, state->score, state->height, state->width);
        for (i = 0; i < state->width * state->height; ++i) {
            fprintf(fp, " %d", (int) state->board[i]);
        }
        fprintf(fp, "\n");
        fclose(fp);
        return 0;
    } else {
        /* Unfortunately this string cannot be made constant because of the %s.
         * At least my compiler warns if I try to do that. */
        printf("Peli %s on jo olemassa\n", name);
        return 1;
    }
}

/* Parses a line from the save file into a GameState. */
static GameState *make_state (const char *line)
{
    GameState *state = NULL;
    long int score;
    int height;
    int width;
    int tmp;
    int i = 0;

    assert(line != NULL);

    /* read score and size */
    if (sscanf(line, "%*s %ld %d %d", &score, &height, &width) != 3) {
        return NULL;
    }

    state = GS_alloc(width, height);
    if (state == NULL) return NULL;
    state->score = score; /* GS_alloc() sets width and height */

    /* move line pointer to beginning of the board */
    for (i = 0; i < 4; ++i) {
        line = strchr(line, ' ');
        if (line == NULL) {
            fprintf(stderr, "Corrupt save file\n");
            return state;
        }
        line++;
    }

    /* read the board */
    i = 0;
    while (sscanf(line, "%d", &tmp) == 1) {
        if (i >= height * width) {
            fprintf(stderr, "Corrupt save file\n");
            break;
        }
        state->board[i] = (unsigned char) tmp;
        i++;

        /* move line pointer to next number */
        line = strchr(line, ' ');
        if (line == NULL) break;
        line++;
    }

    return state;
}

GameState *load_game (const char *name)
{
    FILE *fp = NULL;

    assert(name != NULL);
    fp = fopen(savefile, "r");
    if (fp == NULL) return NULL;

    for (;;) {
        char *line = read_line(fp);
        char buf[256];
        if (line == NULL) break; /* eof reached without finding the save */

        sscanf(line, "%255s", buf);
        if (strcmp(buf, name) == 0) {
            /* found the correct save */
            GameState *state = make_state(line);
            free(line);
            fclose(fp);
            return state;
        }

        free(line);
    }

    fclose(fp);
    printf("Peliä %s ei ole olemassa\n", name);
    return NULL;
}

void list_saves ()
{
    char *line = NULL;
    FILE *fp = fopen(savefile, "r");

    if (fp == NULL) return;

    while ((line = read_line(fp)) != NULL) {
        int i = 0;

        /* save names cannot contain whitespace */
        while (!isspace(line[i])) {
            fputc(line[i], stdout);
            i++;
        }
        fputc('\n', stdout);

        free(line);
    }
    
    fclose(fp);
}

static void copy_file (const char *name, const char *backupname)
{
    FILE *fp, *backup;
    int c;

    fp = fopen(name, "r");
    if (fp == NULL) return;

    backup = fopen(backupname, "w");
    if (backup == NULL) {
        fclose(fp);
        return;
    }

    while ((c = fgetc(fp)) != EOF) {
        fputc(c, backup);
    }

    fclose(fp);
    fclose(backup);
}

/* To delete a save, the game first makes a copy of the save file
 * and then copies all saves _except_ the given one to a new file. */
int delete_save (const char *name)
{
    FILE *old, *new;
    char *line;

    if (!does_save_exist(name)) {
        printf("Peliä %s ei ole olemassa\n", name);
        return 0;
    }

    copy_file(savefile, backupfile);

    old = fopen(backupfile, "r");
    if (old == NULL) return 1;

    new = fopen(savefile, "w");
    if (new == NULL) {
        fclose(old);
        return 1;
    }

    while ((line = read_line(old)) != NULL) {
        char buf[256];

        sscanf(line, "%255s", buf);
        if (strcmp(buf, name) == 0) {
            free(line);
            continue;
        } else {
            fprintf(new, "%s\n", line);
            free(line);
        }
    }

    fclose(old);
    fclose(new);

    return 0;
}
