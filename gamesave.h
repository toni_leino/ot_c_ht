/* Functions for saving/loading game states.
 *
 * The save file contains a line for each saved game state. The format is
 * described in more detail in the documentation. */

#include "gamestate.h"

/* Writes data into the save file. Returns 0 on success. */
int save_game (const GameState *state, const char *name);

/* Loads data from the save file. Returns the created GameState on success
 * and NULL otherwise. */
GameState *load_game (const char *name);

/* Prints all saves found in the save file. */
void list_saves ();

/* Deletes the given save. Returns 0 on success. */
int delete_save (const char *name);
