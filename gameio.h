/* Functions for handling input and output in the game. */

#ifndef GAMEIO_H
#define GAMEIO_H

#include <stdio.h>

#include "gamestate.h"

/* Commands that the game recognizes. */
typedef enum Command
{
    CMD_UNKNOWN, /* command not recognized */
    CMD_RESET, /* start a new game */
    CMD_BOARD, /* print the board */
    CMD_UP,    /* move in the given direction */
    CMD_DOWN,
    CMD_LEFT,
    CMD_RIGHT,
    CMD_SCORE, /* prints the current score */
    CMD_UNDO,  /* undoes a move */
    CMD_REDO,  /* redoes a previously undone move */
    CMD_SAVE,  /* saves the current game state in a file */
    CMD_LOAD,  /* loads a game state from a file */
    CMD_LIST,  /* lists the saves in the save file */
    CMD_DELETE,/* deletes a save from the save file */
    CMD_QUIT   /* quit the game */
} Command;

/* Error message for unrecognized command. */
extern const char *unknown_cmd_error;

/* Message telling the player to start the game first. */
extern const char *game_not_started_msg;

/* Reads a line of any length from a file. */
char *read_line (FILE *fp);

/* Draws the board contained in gs. */
void draw_board (const GameState *gs);

/* Parses a command from input. */
Command input_to_command (const char *input);

#endif /* end of include guard: GAMEIO_H */
