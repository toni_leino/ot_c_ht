#include <stdio.h> /* NULL is defined here */
#include <string.h>
#include <assert.h>

#include "gameexec.h"
#include "gameio.h"
#include "gamestate.h"
#include "gamesave.h"

const int BOARD_W = 4;
const int BOARD_H = 4;

/* Resets the board. The player can give the coordinates and values of the two
 * initial tiles. Returns zero on success. */
static int reset_board (GSList **list, const char *input)
{
    int row1, col1, val1, row2, col2, val2;

    if (*list != NULL) {
        GSList_destroy(*list);
    }

    *list = GSList_alloc();

    if (*list != NULL) {
        /* check if the player gave coordinates */
        if (sscanf(input, "%*s %d %d %d %d %d %d", &row1, &col1, &val1,
                                                   &row2, &col2, &val2) == 6) {
            GameState *state = GS_alloc(BOARD_W, BOARD_H);

            /* add the given tiles, assuming they're not invalid */
            if (value_to_tile(val1) == -1) {
                fprintf(stderr, "invalid tile value %d; must be 2^n\n", val1);
                add_random_tile(state);
                add_random_tile(state);
            } else if (value_to_tile(val2) == -1) {
                fprintf(stderr, "invalid tile value %d; must be 2^n\n", val2);
                add_random_tile(state);
                add_random_tile(state);
            } else {
                /* TODO: check coordinates too */
                add_tile(state, val1, row1, col1);
                add_tile(state, val2, row2, col2);
            }

            return GSList_add(*list, state);
        } else {
            return GSList_add(*list, GS_init(BOARD_W, BOARD_H));
        }
    } else {
        return 1;
    }
}

typedef enum {
    DIR_UP,
    DIR_DOWN,
    DIR_LEFT,
    DIR_RIGHT
} Direction;

/* What happened during the move? */
typedef enum {
    MOVE_OK,     /* no problems */
    MOVE_ERROR,  /* something's wrong with the game */
    MOVE_NOTHING /* nothing happened, don't add a new tile */
} MoveResult;

/*
 * Moving tiles works like this:
 *
 * Let's assume you're moving them right. Every tile (from right to left) except
 * the rightmost one is moved as far right as possible. If there's another tile
 * in the way, they are combined if possible and in any case the tile stops moving.
 * There's an extra check to make sure that if you have a line like 0-8-4-4 it
 * won't combine into a 16 tile: the spots starting from the one stored in the
 * variable "border" are not checked.
 *
 * There's only one function (collapse_line()) for moving tiles; there are a few
 * helper functions for the parts that are direction-specific.
 */

/* Returns the row or column of the tile that should be checked first. */
static int firsttile (Direction dir, const GameState *state) {
    assert(state != NULL);

    if (dir == DIR_LEFT || dir == DIR_UP) {
        return 1;
    } else if (dir == DIR_RIGHT) {
        return state->width - 2;
    } else if (dir == DIR_DOWN) {
        return state->height - 2;
    } else {
        fprintf(stderr, "firsttile() called with invalid direction\n");
        return -1;
    }
}

/* Returns the row or column of the next tile that should be checked. */
static int nexttile (int curr, Direction dir) {
    if (dir == DIR_LEFT || dir == DIR_UP) {
        return curr + 1;
    } else if (dir == DIR_RIGHT || dir == DIR_DOWN) {
        return curr - 1;
    } else {
        fprintf(stderr, "nexttile() called with invalid direction\n");
        return curr;
    }
}

/* Returns 1 if the current tile should not be checked and 0 otherwise. */
static int lasttile (int curr, Direction dir, const GameState *state)
{
    assert(state != NULL);

    if (dir == DIR_RIGHT || dir == DIR_DOWN) {
        return curr < 0;
    } else if (dir == DIR_LEFT) {
        return curr >= state->width;
    } else if (dir == DIR_UP) {
        return curr >= state->height;
    } else {
        fprintf(stderr, "lasttile() called with invalid direction\n");
        return 1;
    }
}

/* Returns the next spot to be checked or -1 if there are no spots left. */
static int nextspot (int curr, Direction dir, const GameState *state)
{
    if (dir == DIR_LEFT || dir == DIR_UP) {
        if (curr <= 0) {
            return -1;
        } else {
            return curr - 1;
        }
    } else if (dir == DIR_RIGHT || dir == DIR_DOWN) {
        if (dir == DIR_RIGHT && curr >= state->width - 1) {
            return -1;
        } else if (dir == DIR_DOWN && curr >= state->height - 1) {
            return -1;
        } else {
            return curr + 1;
        }
    } else {
        fprintf(stderr, "nextspot() called with invalid direction\n");
        return -1;
    }
}

/* Returns the array index of the current spot. */
static int currindex (int curr, int line, Direction dir, const GameState *state)
{
    if (dir == DIR_LEFT || dir == DIR_RIGHT) {
        return array_index(line, curr, state->width);
    } else if (dir == DIR_UP || dir == DIR_DOWN) {
        return array_index(curr, line, state->width);
    } else {
        fprintf(stderr, "currindex() called with invalid direction\n");
        return -1;
    }
}

/* Moves all tiles in the given row or column in the given direction. */
static MoveResult collapse_line (GameState *state, int line, Direction dir)
{
    int tile, spot;
    int tile_index;
    int spot_index, prev_index;
    int move_made = 0; /* 1 if at least one tile was moved */
    int border = -1; /* don't check this spot */

    for (tile = firsttile(dir, state);
         !lasttile(tile, dir, state);
         tile = nexttile(tile, dir)) {
        /* calculate index of current tile */
        tile_index = currindex(tile, line, dir, state);
        spot_index = prev_index = tile_index;

        /* don't try to move empty tiles */
        if (state->board[tile_index] == 0) continue; 

        for (spot = nextspot(tile, dir, state);
             spot != -1;
             spot = nextspot(spot, dir, state)) {
            /* don't check tiles that have already been combined */
            if (spot == border) break;

            /* calculate array index for current and previous spot */
            prev_index = spot_index;
            spot_index = currindex(spot, line, dir, state);

            /* empty spot: move tile there */
            if (state->board[spot_index] == 0) {
                state->board[spot_index] = state->board[prev_index];
                state->board[prev_index] = 0;
                move_made = 1;
            }

            /* non-empty spot: combine tiles if possible, break the inner loop
             * in any case */
            else if (state->board[spot_index] == state->board[prev_index]) {
                /* remember how the values are stored */
                state->board[spot_index] += 1;
                state->board[prev_index] = 0;
                state->score += tile_value(state->board[spot_index]);
                move_made = 1;
                border = spot; /* don't combine this tile twice */
                break;
            } else {
                break;
            }
        }
    }

    return (move_made ? MOVE_OK : MOVE_NOTHING);
}

static MoveResult collapse_row (GameState *state, int row, Direction dir)
{
    assert(state != NULL);
    assert(state->board != NULL);

    if (row < 0 || row >= state->height) return -1;
    if (dir != DIR_LEFT && dir != DIR_RIGHT) return -1;

    return collapse_line(state, row, dir);
}

static MoveResult collapse_col (GameState *state, int col, Direction dir)
{
    assert(state != NULL);
    assert(state->board != NULL);

    if (col < 0 || col >= state->width) return -1;
    if (dir != DIR_UP && dir != DIR_DOWN) return -1;

    return collapse_line(state, col, dir);
}

/* Moves tiles in the given direction. The current game state is not changed;
 * instead the function creates a new state. The player can give the coordinates
 * and value of the next tile. Returns zero on success. */
static int move_tiles (GSList *list, Command cmd, const char *input)
{
    GameState *newstate = NULL;
    MoveResult result = MOVE_ERROR; /* what happened during the move */
    int row, col, val; /* player-given coordinates and value for the next tile */

    assert(list != NULL);
    assert(input != NULL);

    /* check if the game has started */
    if (GSList_getcurr(list) == NULL) {
        return 0;
    }

    /* check if the game is over */
    if (is_game_over(GSList_getcurr(list))) {
        return 0;
    }

    /* copy the current state */
    newstate = GS_copy(GSList_getcurr(list));
    if (newstate == NULL) {
        return 1;
    }

    if (cmd == CMD_LEFT || cmd == CMD_RIGHT) {
        int i;
        for (i = 0; i < newstate->height; ++i) {
            MoveResult newresult = collapse_row(newstate, i,
                                 (cmd == CMD_LEFT) ? DIR_LEFT : DIR_RIGHT);
            if (newresult == MOVE_ERROR) {
                fprintf(stderr, "something's wrong in move_tiles()\n");
            } else if (newresult == MOVE_OK) {
                result = MOVE_OK;
            }
        }
    } else if (cmd == CMD_UP || cmd == CMD_DOWN) {
        int i;
        for (i = 0; i < newstate->width; ++i) {
            MoveResult newresult = collapse_col(newstate, i,
                                 (cmd == CMD_UP) ? DIR_UP : DIR_DOWN);
            if (newresult == MOVE_ERROR) {
                fprintf(stderr, "something's wrong in move_tiles()\n");
            } else if (newresult == MOVE_OK) {
                result = MOVE_OK;
            }
        }
    } else {
        fprintf(stderr, "move_tiles() called with invalid command\n");
        return 1;
    }

    if (result == MOVE_OK) {
        if (sscanf(input, "%*s %d %d %d", &row, &col, &val) == 3) {
            if (value_to_tile(val) != -1) {
                add_tile(newstate, val, row, col);
            }
        } else {
            add_random_tile(newstate);
        }

        GSList_add(list, newstate);
    } else {
        GS_free(newstate);
    }
    return (result == MOVE_ERROR ? 1 : 0);
}

/* Parses the input and saves the current state with the given name. */
static int save (const GameState *state, const char *input)
{
    char buf[256]; /* this should be enough for the save name */

    if (sscanf(input, "%*s %255s", buf) == 1) {
        return save_game(state, buf);
    } else { /* no save name found in input */
        return 0;
    }
}

/* Parses the input and loads the given save. Returns zero on success. */
static int load (GSList **list, const char *input)
{
    char buf[256];

    if (sscanf(input, "%*s %255s", buf) == 1) {
        GameState *state = load_game(buf);
        if (state == NULL) return 0; /* no save with the given name */

        if (*list != NULL) {
            GSList_destroy(*list);
        }

        *list = GSList_alloc();

        if (*list == NULL) return 1;

        return GSList_add(*list, state);
    } else { /* no save name found in input */
        return 0;
    }
}

/* Parses the input and removes the given save, if it exists. */
int delete (const char *input)
{
    char buf[256];

    if (sscanf(input, "%*s %255s", buf) == 1) {
        return delete_save(buf);
    } else {
        return 0;
    }
}

int exec_command (GSList **list, Command cmd, const char *input)
{
    if (!(cmd == CMD_RESET || cmd == CMD_LIST || cmd == CMD_LOAD || cmd == CMD_DELETE)
        && *list == NULL) {
        printf("%s", game_not_started_msg);
        return 0;
    }

    switch (cmd) {
        case CMD_RESET:
            reset_board(list, input);
            break;
        case CMD_BOARD:
            draw_board(GSList_getcurr(*list));
            break;
        case CMD_UP:
        case CMD_DOWN:
        case CMD_LEFT:
        case CMD_RIGHT:
            move_tiles(*list, cmd, input);
            break;
        case CMD_SCORE:
            printf("%ld\n", GSList_getcurr(*list)->score);
            return 0;
            break;
        case CMD_UNDO:
            return GSList_undo(*list);
            break;
        case CMD_REDO:
            return GSList_redo(*list);
            break;
        case CMD_SAVE:
            return save(GSList_getcurr(*list), input);
            break;
        case CMD_LOAD:
            return load(list, input);
            break;
        case CMD_LIST:
            list_saves();
            return 0;
            break;
        case CMD_DELETE:
            return delete(input);
            break;
        default:
            return 1;
            break;
    }

    return 1;
}
