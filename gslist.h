/* GSList is a doubly linked list of GameStates. It's used for the undo history. */

#ifndef GSLIST_H
#define GSLIST_H

#include "gamestate.h"

/* One node of the linked list. */
typedef struct GSLNode
{
    GameState *state;
    struct GSLNode *next;
    struct GSLNode *prev;
} GSLNode;

/* The complete linked list. It contains pointers to the oldest game state,
 * the newest one and the current one. */
typedef struct GSList
{
    GSLNode *oldest;
    GSLNode *newest;
    GSLNode *curr;
} GSList;

/* Creates an empty GSList. */
GSList *GSList_alloc (void);

/* Frees a GSList as well as all the nodes and states in it. */
void GSList_destroy (GSList *list);

/* Adds a new GameState after the current state. (_Not_ to the end of the list!)
 * Frees all the nodes and states that come after curr before adding the new one.
 * Returns zero if successful and non-zero otherwise. */
int GSList_add (GSList *list, GameState *state);

/* Redoes / undoes one move. Returns zero if successful. */
int GSList_undo (GSList *list);
int GSList_redo (GSList *list);

/* Returns a pointer to the current state. */
GameState *GSList_getcurr (const GSList *list);

#endif /* end of include guard: GSLIST_H */
