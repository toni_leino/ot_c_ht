#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "gameio.h"
#include "gamestate.h"

const char *unknown_cmd_error = "Tuntematon komento!\n";
const char *game_not_started_msg = "Aloita peli ensin! (resetoi)\n";
const char *game_over_msg = "Peli on päättynyt!\n";

char *read_line (FILE *fp)
{
    size_t bufsize = 256; /* space allocated for buf */
    size_t buflen = 0; /* number of characters (excluding '\0') in buf */
    char *buf = malloc(bufsize);
    int c;

    assert(fp != NULL);
    if (buf == NULL) return NULL;

    for (;;) {
        c = fgetc(fp);

        /* check for newline or EOF */
        if (c == '\n') {
            break;
        } else if (c == EOF) {
            /* return NULL if nothing was read before EOF */
            if (buflen == 0) {
                free(buf);
                return NULL;
            } else {
                break;
            }
        }

        /* Request more space if needed. Keep space for the ending \0 too */
        if (buflen == bufsize - 2) {
            bufsize *= 2;
            buf = realloc(buf, bufsize);
        }

        buf[buflen] = c;
        buflen++;
    }

    /* add \0 and free extra space */
    buf[buflen] = '\0';
    buf = realloc(buf, buflen + 1);

    return buf;
}

void draw_board (const GameState *gs)
{
    int i;
    int board_size = gs->width * gs->height;

    if (gs == NULL) return;

    for (i = 0; i < board_size; ++i) {
        /* print the tile */
        if (gs->board[i] != 0) {
            printf("%5d", tile_value(gs->board[i]));
        } else {
            printf("     ");
        }

        /* change line after printing a complete row */
        if ((i + 1) % gs->width == 0) {
            printf("\n");
        }
    }

    if (is_game_over(gs)) {
        printf("%s", game_over_msg);
    }
}

Command input_to_command (const char *input)
{
    /* let's assume that the commands won't be longer than 31 characters... */
    char tmp[32];

    /* we're only interested in the first word here */
    sscanf(input, "%31s", tmp);

#define SAME(a, b) (strcmp(a, b) == 0)
    if (SAME(tmp, "lopeta")) {
        return CMD_QUIT;
    } else if (SAME(tmp, "resetoi")) {
        return CMD_RESET;
    } else if (SAME(tmp, "tilanne")) {
        return CMD_BOARD;
    } else if (SAME(tmp, "ylös")) {
        return CMD_UP;
    } else if (SAME(tmp, "alas")) {
        return CMD_DOWN;
    } else if (SAME(tmp, "vasemmalle")) {
        return CMD_LEFT;
    } else if (SAME(tmp, "oikealle")) {
        return CMD_RIGHT;
    } else if (SAME(tmp, "pisteet")) {
        return CMD_SCORE;
    } else if (SAME(tmp, "undo")) {
        return CMD_UNDO;
    } else if (SAME(tmp, "redo")) {
        return CMD_REDO;
    } else if (SAME(tmp, "tallenna")) {
        return CMD_SAVE;
    } else if (SAME(tmp, "lataa")) {
        return CMD_LOAD;
    } else if (SAME(tmp, "listaa")) {
        return CMD_LIST;
    } else if (SAME(tmp, "poista")) {
        return CMD_DELETE;
    } else {
        return CMD_UNKNOWN;
    }
#undef SAME
}
