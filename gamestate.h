/* Functions and other stuff related to the game state: the board,
 * score etc. */

#ifndef GAMESTATE_H
#define GAMESTATE_H

/*
 * This struct holds all the information that needs to be known about the
 * current state of the game.
 *
 * The board is stored as an 1D array where indices 0-width are the first row,
 * indices width-(2*width+1) are the second row etc.
 *
 * To save memory, the tile values are not stored directly; instead, the game
 * stores log2(n) where n is the value of the tile. So the tile value 2 is
 * represented by 1, 4 is 2, 8 is 3 and so on.
 */

typedef struct GameState {
    /* Board array. */
    unsigned char *board;

    /* Size of the board. */
    int width;
    int height;

    /* The player's current score. Long just in case someone tries to play this
     * on a system where int is 16-bit. */
    unsigned long int score;
} GameState;

/*
 * Functions for creating / deleting game states
 */

/* Initializes memory for the game state. */
GameState *GS_alloc (int width, int height);
/* Creates the initial game state, which contains two random tiles. */
GameState *GS_init (int width, int height);
/* Creates a new game state by copying another one. */
GameState *GS_copy (GameState *gs);
/* Destroys a game state. */
void GS_free (GameState *gs);

/*
 * Miscellaneous functions
 */

/* Adds a random 2 or 4 tile in an empty spot. */
void add_random_tile (GameState *gs);

/* Adds a tile with the given value in the given position. */
void add_tile (GameState *gs, int value, int row, int col);

/* Returns non-zero if the board has no empty spots left. */
int is_board_full (const GameState *gs);

/* Returns non-zero if there are no possible moves. */
int is_game_over (const GameState *gs);

/* Calculates the value of a tile. */
int tile_value (unsigned char tile);

/* Reverse of the above: calculates the stored tile based on the value.
 * Returns -1 on error. */
int value_to_tile (int value);

/* Calculates the correct index for position (row, col).
 * Remember that the board is stored as a 1D array. */
int array_index (int row, int col, int width);

#endif /* end of include guard: GAMESTATE_H */
